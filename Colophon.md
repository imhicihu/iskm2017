* Software
    - [Duplicati](https://www.duplicati.com/) (open-source backup software)
    - [CanIuse](https://caniuse.com/): browser/browsing test tool
    - [Jest](https://facebook.github.io/jest/en/) (javascript testing tool)
    - [Atom](https://atom.io) (code editor) plus lots of packages
    - [Brackets](http://brackets.io/) (code editor)
    
    

* Hardware
    - In this project we use this hardware (not mandatory):
        - Macbook 15"
        - iMac 21"
        - 21" Dell monitor