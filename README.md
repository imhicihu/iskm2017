![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/3278295154-status_archived.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-closed](https://bitbucket.org/repo/ekyaeEE/images/1555006384-issues_closed.png)


# Rationale #

* This document is related to the [website](http://www.imhicihu-conicet.gob.ar/iskm2017/) of ISKM 2017. 
* The (internal) source and commits & the workflow _et alia_ is not shared on behalf of security (up to now). 
* Please, send any inquiry to this [email](mailto:imhicihu@gmail.com) or our [Board]((https://bitbucket.org/imhicihu/iskm2017/addon/trello/trello-board)).

![iskm2017-web.png](https://bitbucket.org/repo/bBMkd4/images/892361199-iskm2017-web.png)


### What is this repository for? ###

* Worflow in the making of [http://www.imhicihu-conicet.gob.ar/iskm2017/](http://www.imhicihu-conicet.gob.ar/iskm2017/) ~~webapps, mobile content, etc.~~
* Version 1.3

### How do I get set up? ###

* Summary of set up
     - an Apache server. A _legit_ CPanel account. A [CMS](https://en.wikipedia.org/wiki/Content_management_system) to install. 
* Dependencies
     - _the less, the better_. A personal [motto](http://dictionary.cambridge.org/es/diccionario/ingles/motto)
* Database configuration
     - the same CMS provides an inner control- self-automatic system. 
     - a weekly backup is done for third-parties to prevent corruption but mostly for *security* reasons. 
     - a monthly backup is programmed via Cpanel (FTP or via internal File Manager)
* How to run tests
     - No data is provided. From the beginning to the final of this project, some privacy & security issues must be fulfilled.
* Deployment instructions
     - This repo is a project related with a [symposium](http://www.imhicihu-conicet.gob.ar/iskm2017/). So, this _rationale_ begins and ends with it.

### Related repositories

* [ISKM2017 - Mobile App](https://bitbucket.org/imhicihu/iskm2017-mobile-app/src/master/)
* [One Page website](https://bitbucket.org/imhicihu/one-page-website/src/master/)
* [Conferences](https://bitbucket.org/imhicihu/conferences/src/master/)
* [Streaming](https://bitbucket.org/imhicihu/streaming/src/master/)
* [Temas Medievales project](https://bitbucket.org/imhicihu/temas-medievales-project/src/master/)
* [Presentations (norms, checklist, proxies)](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/iskm2017/src)

### Issues ###

* ~~On behalf of security, they can't be shared. Open to browse until 2018-15-10~~

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/iskm2017/commits/) section for the current status

### Contribution guidelines ###

* Code review
     - For *security* reasons, there is no code to share. 
     - What it can be shared can be found in the [Downloads](https://bitbucket.org/imhicihu/iskm2017/downloads/) or [Source](https://bitbucket.org/imhicihu/iskm2017/src) section.

### Who do I talk to? ###

* Repo owner or admin:
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/iskm2017/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/iskm2017/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### Licence ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)